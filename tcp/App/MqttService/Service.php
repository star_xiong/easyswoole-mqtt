<?php


namespace App\MqttService;
use App\Model\DeviceModel;

class Service
{

    public function parse(\Swoole\Server  $server, int $fd, int $reactor_id, $data)
    {
        // $result = DeviceModel::create()->all();
        // var_dump($result);
        $data = json_encode($data);
        echo "fd:{$fd} reactor_id:{$reactor_id} send:{$data}\n";
    }
    
    public function login(int $fd, int $reactor_id, $data)
    {
        // $result = DeviceModel::create()->all();
        // var_dump($result);
        //$data = json_encode($data);
        echo "fd:{$fd} Login\n";
        return true;
    }
}
