<?php

namespace App\Model;

use EasySwoole\ORM\AbstractModel;

/**
 * Class AdminModel
 * Create With Automatic Generator
 * @property $adminId
 * @property $adminName
 * @property $adminAccount
 * @property $adminPassword
 * @property $adminSession
 * @property $adminLastLoginTime
 * @property $adminLastLoginIp
 */
class DeviceModel extends AbstractModel
{
    protected $tableName = 'sevice';

    protected $primaryKey = 'id';

}