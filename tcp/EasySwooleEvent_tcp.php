<?php


namespace EasySwoole\EasySwoole;


use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\FastCache\Cache;
use EasySwoole\ORM\DbManager;
use EasySwoole\ORM\Db\Connection;
use App\TcpServer\Service;


class EasySwooleEvent implements Event
{
    public static function initialize()
    {
        date_default_timezone_set('Asia/Shanghai');
        
        // 实现 onRequest 事件
        \EasySwoole\Component\Di::getInstance()->set(\EasySwoole\EasySwoole\SysConst::HTTP_GLOBAL_ON_REQUEST, function (\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response): bool {
            ###### 对请求进行拦截 ######
            // 不建议在这拦截请求，可增加一个控制器基类进行拦截
            // 如果真要拦截，判断之后 return false; 即可
            /*
            $code = $request->getRequestParam('code');
            if (0){ // empty($code)验证失败
                $data = array(
                    "code" => \EasySwoole\Http\Message\Status::CODE_BAD_REQUEST,
                    "result" => [],
                    "msg" => '验证失败'
                );
                $response->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                $response->withHeader('Content-type', 'application/json;charset=utf-8');
                $response->withStatus(\EasySwoole\Http\Message\Status::CODE_BAD_REQUEST);
                return false;
            }
            return true;
            */

            ###### 处理请求的跨域问题 ######
            $response->withHeader('Access-Control-Allow-Origin', '*');
            $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            $response->withHeader('Access-Control-Allow-Credentials', 'true');
            $response->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
            if ($request->getMethod() === 'OPTIONS') {
                $response->withStatus(\EasySwoole\Http\Message\Status::CODE_OK);
                return false;
            }
            return true;
        });
        
        // 实现 afterRequest 事件
        \EasySwoole\Component\Di::getInstance()->set(\EasySwoole\EasySwoole\SysConst::HTTP_GLOBAL_AFTER_REQUEST, function (\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response): void {

            // 示例：获取此次请求响应的内容
            //TrackerManager::getInstance()->getTracker()->endPoint('request');
            $responseMsg = $response->getBody()->__toString();
            Logger::getInstance()->console('响应内容:' . $responseMsg);
            // 响应状态码：
            // var_dump($response->getStatusCode());

            // tracker 结束，结束之后，能看到中途设置的参数，调用栈的运行情况
            //TrackerManager::getInstance()->closeTracker();
        });
    }

    public static function mainServerCreate(EventRegister $register)
    {
         ###### 注册 mysql orm 连接池 ######
        $config = new \EasySwoole\ORM\Db\Config(Config::getInstance()->getConf('MYSQL'));
        // 【可选操作】我们已经在 dev.php 中进行了配置
        # $config->setMaxObjectNum(20); // 配置连接池最大数量
        DbManager::getInstance()->addConnection(new Connection($config));
        
        //子服务，另开一个端口进行tcp监听
        $server = \EasySwoole\EasySwoole\ServerManager::getInstance()->getSwooleServer();

        $subPort = $server->addlistener('0.0.0.0', 9510, SWOOLE_TCP);
        
        $subPort->set([
            // swoole 相关配置
            'open_length_check' => false,
        ]);
        $subPort->on($register::onConnect, function (\Swoole\Server $server, int $fd, int $reactor_id) {
                echo "fd {$fd} connected\n";
        });
    
        //处理收到的消息
        $service = new Service();
        $subPort->on($register::onReceive, function (\Swoole\Server  $server, int $fd, int $reactor_id, string $data)use($service) {
            $service->parse($server, $fd, $reactor_id, $data);
            //$server->send($fd,"01");
            //echo "fd:{$fd} send:{$data}\n";
        });
        $subPort->on($register::onClose, function (\Swoole\Server  $server, int $fd, int $reactor_id) {
                echo "fd {$fd} closed\n";
        });

    }
}