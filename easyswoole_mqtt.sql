/*
 Navicat Premium Data Transfer

 Source Server         : 81.69.254.159
 Source Server Type    : MySQL
 Source Server Version : 50650
 Source Host           : 81.69.254.159:3306
 Source Schema         : easyswoole_mqtt

 Target Server Type    : MySQL
 Target Server Version : 50650
 File Encoding         : 65001

 Date: 28/05/2022 16:57:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_list
-- ----------------------------
DROP TABLE IF EXISTS `admin_list`;
CREATE TABLE `admin_list`  (
  `adminId` int(11) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `adminAccount` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `adminPassword` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `adminSession` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `adminLastLoginTime` int(11) NULL DEFAULT NULL,
  `adminLastLoginIp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`adminId`) USING BTREE,
  UNIQUE INDEX `adminAccount`(`adminAccount`) USING BTREE,
  INDEX `adminSession`(`adminSession`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_list
-- ----------------------------
INSERT INTO `admin_list` VALUES (1, 'admin name', 'admin', '$2y$10$gX/3kyHlXrEayORGfYUCt.q6da0WJqcOPUTjlTRTxwHF.YHKvNx8y', '9921abfd63ee2347fc6c62e26173baff', 1624007128, '119.128.153.250');

-- ----------------------------
-- Table structure for banner_list
-- ----------------------------
DROP TABLE IF EXISTS `banner_list`;
CREATE TABLE `banner_list`  (
  `bannerId` int(11) NOT NULL AUTO_INCREMENT,
  `bannerName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bannerImg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'banner图片',
  `bannerDescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bannerUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `state` tinyint(3) NULL DEFAULT NULL COMMENT '状态0隐藏 1正常',
  PRIMARY KEY (`bannerId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of banner_list
-- ----------------------------
INSERT INTO `banner_list` VALUES (1, '测试banner', 'asdadsasdasd.jpg', '测试的banner数据', 'www.php20.cn', 1);

-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS `device`;
CREATE TABLE `device`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '监控任务',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '因子类型',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数采名称',
  `mianji` decimal(10, 2) NOT NULL COMMENT '水池截面积',
  `uid` int(11) NOT NULL,
  `imei` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mn` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pw` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备品牌',
  `online` tinyint(1) NOT NULL COMMENT '是否在线:1在线',
  `authentication` tinyint(1) NOT NULL COMMENT '是否认证:1认证',
  `created_at` int(11) NOT NULL,
  `last_login` int(11) NOT NULL COMMENT '检测时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of device
-- ----------------------------
INSERT INTO `device` VALUES (1, '重点排水户末端监控-', '污水检测井-', '01', 0.00, 101, '', '-', '123456', '', '', 0, 0, 0, 1631537881);
INSERT INTO `device` VALUES (2, '零星废水产生企业1', '零星废水产生企业-', '01', 0.00, 100, '', 'zhuji', '123456', '', '', 0, 0, 0, 1632037801);
INSERT INTO `device` VALUES (3, '市级以上VOCs过程监控', '', '01', 0.00, 101, '', 'fenji', '123456', '', '', 0, 0, 0, 1632029945);
INSERT INTO `device` VALUES (111, '', '123', '01', 0.00, 101, '', 'zhujiGN', '1234567', '', '测试主机', 0, 0, 0, 1635298724);
INSERT INTO `device` VALUES (112, '', 'fenji1', '01', 0.00, 101, '', 'cheshi2', '123456', '', '测试分机', 0, 0, 0, 1634539459);
INSERT INTO `device` VALUES (113, '', 'fenji2', '01', 0.00, 101, '', 'cheshi3', '123456', '', '测试分机', 0, 0, 0, 1634539708);
INSERT INTO `device` VALUES (114, '零星废水产生企业', '零星废水产生企业', '01', 0.00, 100, '863488057887898', 'LX202109270008', '123456', '', '采源', 0, 0, 0, 1635299006);
INSERT INTO `device` VALUES (115, '重点排水户末端监控', '污水检测井', '01', 0.00, 101, '868739057381240', 'PS202109160010', '123456', '', '锦添', 0, 0, 0, 1635238765);

-- ----------------------------
-- Table structure for device_record
-- ----------------------------
DROP TABLE IF EXISTS `device_record`;
CREATE TABLE `device_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `imei` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` int(8) NULL DEFAULT NULL,
  `v1` float NULL DEFAULT NULL,
  `v2` float NULL DEFAULT 0,
  `v3` float NULL DEFAULT NULL,
  `v4` float NULL DEFAULT NULL,
  `v5` float NULL DEFAULT NULL,
  `v6` float NULL DEFAULT NULL,
  `v7` float NULL DEFAULT NULL,
  `topic` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 884942 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of device_record
-- ----------------------------

-- ----------------------------
-- Table structure for factor
-- ----------------------------
DROP TABLE IF EXISTS `factor`;
CREATE TABLE `factor`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '监控任务',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '因子类型',
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '因子编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of factor
-- ----------------------------

-- ----------------------------
-- Table structure for send_data
-- ----------------------------
DROP TABLE IF EXISTS `send_data`;
CREATE TABLE `send_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `type` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35398 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of send_data
-- ----------------------------

-- ----------------------------
-- Table structure for user_list
-- ----------------------------
DROP TABLE IF EXISTS `user_list`;
CREATE TABLE `user_list`  (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userAccount` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userPassword` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `addTime` int(11) NULL DEFAULT NULL,
  `lastLoginIp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lastLoginTime` int(10) NULL DEFAULT NULL,
  `userSession` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` tinyint(2) NULL DEFAULT NULL,
  `money` int(10) NOT NULL DEFAULT 0 COMMENT '用户余额',
  `frozenMoney` int(10) NOT NULL DEFAULT 0 COMMENT '冻结余额',
  `company` int(11) NOT NULL,
  `deviceId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据项的值，数据单位按“NECC-NHJC-02”要求',
  `enterpriseCode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '统一社会信用代码',
  `region` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dataCode` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '上传数据项编码（参照NECC-NHJC-02）',
  `inputType` tinyint(4) NOT NULL COMMENT '数据采集类型：1管理信息系统；2生产监控管理系统；3分布式控制系统；4现场仪表；5手工填报	',
  `statType` tinyint(1) NOT NULL COMMENT '数据采集频率：0 实时、1 日、2 月、3 年',
  `statDate` date NOT NULL COMMENT '数据统计时间 yyyy-MM-dd HH:mm:ss ，为数据统计周期的起始时间',
  `scope` tinyint(1) NOT NULL COMMENT '数据范围：1全厂；2生产工序；3生产工序单元；4重点耗能设备',
  `sn` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`userId`) USING BTREE,
  UNIQUE INDEX `pk_userAccount`(`userAccount`) USING BTREE,
  INDEX `userSession`(`userSession`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_list
-- ----------------------------
INSERT INTO `user_list` VALUES (100, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '', '', '', '', 0, 0, '0000-00-00', 0, '');
INSERT INTO `user_list` VALUES (101, 'xsk', 'haineng', '21218cca77804d2ba1922c33e0151105', '13553810187', NULL, '', 1628297575, '', 1, 10000, 0, 0, '', '', '', '', 0, 0, '0000-00-00', 0, '');

SET FOREIGN_KEY_CHECKS = 1;
