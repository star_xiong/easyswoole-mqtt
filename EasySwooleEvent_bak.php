<?php
/**SIMPS/MQTT/V5 服务端**/

namespace EasySwoole\EasySwoole;

use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\FastCache\Cache;
use EasySwoole\ORM\DbManager;
use EasySwoole\ORM\Db\Connection;
use App\MqttService\Service;
use Simps\MQTT\Protocol\Types;
use Simps\MQTT\Protocol\V3;
use Simps\MQTT\Tools\Common;
use EasySwoole\EasySwoole\Crontab\Crontab;  
use App\Crontab\InstantData;
use App\Crontab\MinuteCron;
use App\Crontab\HourlyData;
use App\Crontab\DailyData;

class EasySwooleEvent implements Event
{
    public static function initialize()
    {
        date_default_timezone_set('Asia/Shanghai');
        
        // 实现 onRequest 事件
        \EasySwoole\Component\Di::getInstance()->set(\EasySwoole\EasySwoole\SysConst::HTTP_GLOBAL_ON_REQUEST, function (\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response): bool {
            ###### 对请求进行拦截 ######
            // 不建议在这拦截请求，可增加一个控制器基类进行拦截
            // 如果真要拦截，判断之后 return false; 即可
            /*
            $code = $request->getRequestParam('code');
            if (0){ // empty($code)验证失败
                $data = array(
                    "code" => \EasySwoole\Http\Message\Status::CODE_BAD_REQUEST,
                    "result" => [],
                    "msg" => '验证失败'
                );
                $response->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                $response->withHeader('Content-type', 'application/json;charset=utf-8');
                $response->withStatus(\EasySwoole\Http\Message\Status::CODE_BAD_REQUEST);
                return false;
            }
            return true;
            */

            ###### 处理请求的跨域问题 ######
            $response->withHeader('Access-Control-Allow-Origin', '*');
            $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            $response->withHeader('Access-Control-Allow-Credentials', 'true');
            $response->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
            if ($request->getMethod() === 'OPTIONS') {
                $response->withStatus(\EasySwoole\Http\Message\Status::CODE_OK);
                return false;
            }
            return true;
        });
        
        // 实现 afterRequest 事件
        \EasySwoole\Component\Di::getInstance()->set(\EasySwoole\EasySwoole\SysConst::HTTP_GLOBAL_AFTER_REQUEST, function (\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response): void {

            // 示例：获取此次请求响应的内容
            //TrackerManager::getInstance()->getTracker()->endPoint('request');
            $responseMsg = $response->getBody()->__toString();
            Logger::getInstance()->console('响应内容:' . $responseMsg);
            // 响应状态码：
            // var_dump($response->getStatusCode());

            // tracker 结束，结束之后，能看到中途设置的参数，调用栈的运行情况
            //TrackerManager::getInstance()->closeTracker();
        });
    }

    public static function mainServerCreate(EventRegister $register)
    {
        ###### 1. 注册 mysql orm 连接池 Start ######
        $config = new \EasySwoole\ORM\Db\Config(Config::getInstance()->getConf('MYSQL'));
        // 【可选操作】我们已经在 dev.php 中进行了配置
        # $config->setMaxObjectNum(20); // 配置连接池最大数量
        DbManager::getInstance()->addConnection(new Connection($config));
        
        ###### 2. 注册 redis pool 连接池 使用请看 redis 章节文档 ######
        $redisConfig = new \EasySwoole\Redis\Config\RedisConfig(
            [
                'host' => '127.0.0.1', // 服务端地址 默认为 '127.0.0.1'
                'port' => 6379, // 端口 默认为 6379
                'auth' => '', // 密码 默认为 不设置
                'db'   => 0, // 默认为 0 号库
            ]
        );
        //redis连接池注册(config默认为127.0.0.1,端口6379)
        $redisPoolConfig = \EasySwoole\RedisPool\RedisPool::getInstance()->register($redisConfig);
        //配置连接池连接数
        $redisPoolConfig->setMinObjectNum(5);
        $redisPoolConfig->setMaxObjectNum(20);
        // go(function() {
        //     $redis = \EasySwoole\RedisPool\RedisPool::defer();
        //     $redis->flushall();
        // });
        
        // $redisPoolConfig->recycle();
        
        //         $scheduler = new \Swoole\Coroutine\Scheduler();
        //         $scheduler->add(function() {
        //             $redis = \EasySwoole\RedisPool\RedisPool::defer();
        //             $redis->flushall();
        //         });
        //         $scheduler->start();
        // // 清除全部定时器
        // \Swoole\Timer::clearAll();

        ###### 3. 注册定时任务 ######
        // Crontab::getInstance()->addTask(InstantData::class);    //实时数据
        Crontab::getInstance()->addTask(MinuteCron::class);     //分钟数据
        Crontab::getInstance()->addTask(HourlyData::class);     //小时数据
        Crontab::getInstance()->addTask(DailyData::class);      //日数据
        
        
        ###### 4. 注册进程（注册TCP客户端进程） ######
        $tcpPost = [
            6000,
            5600,
            // 3100,
            // 5500,
            // 5700,
            // 5800,
            // 5900,
            // 6100,
            // 6200,
            // 6300
        ];
        foreach($tcpPost as $post){
            $processName = "TcpClient{$post}";
            $processConfig = new \EasySwoole\Component\Process\Config([
                'processName' => $processName, // 设置 进程名称为 TickProcess
                'processGroup' => 'TcpClient', // 设置 进程组名称为 Tick
                'arg' => [
                    'post' => $post,
                    'clientId' => '119.145.97.35',
                    'timeout' => 5,     //5秒超时
                ], // 传递参数到自定义进程中
                'enableCoroutine' => true, // 设置 自定义进程自动开启协程环境
            ]);
    
            // 【推荐】使用 \EasySwoole\Component\Process\Manager 类注册自定义进程
            $tcpClientProcess = (new \App\MqttService\TcpClient($processConfig));
            // 【可选操作】把 tickProcess 的 Swoole\Process 注入到 Di 中，方便在后续控制器等业务中给自定义进程传输信息(即实现主进程与自定义进程间通信)
            \EasySwoole\Component\Di::getInstance()->set($processName, $tcpClientProcess->getProcess());
            // 注册进程
            \EasySwoole\Component\Process\Manager::getInstance()->addProcess($tcpClientProcess);
        }
        

        ###### 5. 注册子服务，另开一个端口进行tcp监听 ######
        $server = \EasySwoole\EasySwoole\ServerManager::getInstance()->getSwooleServer();
        $subPort = $server->addlistener('0.0.0.0', 9510, SWOOLE_BASE);
        $subPort->set([
            // swoole 相关配置
            'open_mqtt_protocol' => true,
            'worker_num' => 2,
            'package_max_length' => 2 * 1024 * 1024,
        ]);
        
        //客户端连接
        $subPort->on($register::onConnect, function (\Swoole\Server $server, int $fd, int $reactor_id) {
            echo "Client #{$fd}: Connected.\n";
        });
        
        //处理收到的消息
        $service = new Service();
        $subPort->on($register::onReceive, function (\Swoole\Server  $server, int $fd, int $reactor_id, $data)use($service) {
            try {
                // debug
                //        Common::printf($data);
                $data = V3::unpack($data);
                //var_dump($data);
                
                if (is_array($data) && isset($data['type'])) {
                    switch ($data['type']) {
                        case Types::CONNECT:
                            // Check protocol_name
                            if ($data['protocol_name'] != 'MQTT') {
                                $server->close($fd);
                                return false;
                            }
                            
                            //客户端登录
                            if($service->login($fd, $reactor_id, $data) == false){
                                $server->close($fd);
                                return false;
                            }
                            // Check connection information, etc.
        
                            $server->send(
                                $fd,
                                V3::pack(
                                    [
                                        'type' => Types::CONNACK,
                                        'code' => 0,
                                        'session_present' => 0,
                                    ]
                                )
                            );
                            break;
                        case Types::PINGREQ:
                            $server->send($fd, V3::pack(['type' => Types::PINGRESP]));
                            break;
                        case Types::DISCONNECT:
                            if ($server->exist($fd)) {
                                $server->close($fd);
                            }
                            break;
                        case Types::PUBLISH:
                            // Send to subscribers
                            //处理收到的消息
                            //echo sprintf('0x%x',$data['message']);
                            //hex_dump($data['message']);
                            $service->parse($server, $fd, $reactor_id, $data);
                            foreach ($server->connections as $sub_fd) {
                                if ($sub_fd != $fd) {
                                    $server->send(
                                        $sub_fd,
                                        V3::pack(
                                            [
                                                'type' => $data['type'],
                                                'topic' => $data['topic'],
                                                'message' => $data['message'],
                                                'dup' => $data['dup'],
                                                'qos' => $data['qos'],
                                                'retain' => $data['retain'],
                                                'message_id' => $data['message_id'] ?? '',
                                            ]
                                        )
                                    );
                                }
                            }
        
                            if ($data['qos'] === 1) {
                                $server->send(
                                    $fd,
                                    V3::pack(
                                        [
                                            'type' => Types::PUBACK,
                                            'message_id' => $data['message_id'] ?? '',
                                        ]
                                    )
                                );
                            }
        
                            break;
                        case Types::SUBSCRIBE:
                            $payload = [];
                            foreach ($data['topics'] as $k => $qos) {
                                if (is_numeric($qos) && $qos < 3) {
                                    $payload[] = $qos;
                                } else {
                                    $payload[] = 0x80;
                                }
                            }
                            $server->send(
                                $fd,
                                V3::pack(
                                    [
                                        'type' => Types::SUBACK,
                                        'message_id' => $data['message_id'] ?? '',
                                        'codes' => $payload,
                                    ]
                                )
                            );
                            break;
                        case Types::UNSUBSCRIBE:
                            $server->send(
                                $fd,
                                V3::pack(
                                    [
                                        'type' => Types::UNSUBACK,
                                        'message_id' => $data['message_id'] ?? '',
                                    ]
                                )
                            );
                            break;
                    }
                } else {
                    $server->close($fd);
                }
            } catch (\Throwable $e) {
                echo "\033[0;31mError: {$e->getMessage()}\033[0m\r\n";
                $server->close($fd);
            }
        });

        $subPort->on($register::onClose, function (\Swoole\Server  $server, int $fd, int $reactor_id) {
                $redis = \EasySwoole\RedisPool\RedisPool::defer();
                $redis->del("fd_device_id_{$fd}");
                $redis->del("fd_client_id_{$fd}");
                echo "Client #{$fd}: Closed.\n";
        });

    }

}