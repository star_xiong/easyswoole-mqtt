启动服务:nohup php easyswoole server start &
关闭服务:php easyswoole stop force
杀死占用端口的进程:kill `lsof -t -i:9510`

## git 自动将本地代码同步到线上服务器

### 1、创建一个文件夹,用来存放git仓库

`mkdir git_rep`

`cd git_rep`

### 2、初始化(创建)一个git仓库:git init --bare 目录(仓库)名称

`git init --bare localhost`

### 3、修改目录权限

`chmod -R 777 /get_rep/localhost/`

### 4、创建一个用户,克隆仓库和提交代码时就用它

`useradd qiming`
`passwd qiming`

### 5、在本地打开 git bash here

### 6、将线上仓库克隆到本地,克隆要输入刚才密码

`$ git clone qiming@ip:/git_rep/localhost baiduyun`
`$ git clone ssh://starmqtt@81.69.254.159:22/data/git_rep/easyswoole_mqtt`

### 7、有可能在克隆的时候会出错,百度解决错误

### 8、编辑钩子函数,让仓库的代码同步到网站目录[linux]

`cd /git_rep/localhost/hook/`
`mv post-update.sample post-update`
`vim post-update`

``#!/bin/sh``
``git --work-tree=/www/wwwroot/localhost checkout -f``
``git push origin master``

### 9、修改你要同步到哪里的目录权限,否则有可能同步不了

### 到此完成了仓库代码自动同步到网站目录了,但是这时候本地提交还是需要输入繁琐的命令

`$ cd localhost`
`$ git add .`
`$ git commit -m 'msg'`
`$ git push origin master`

### 10、简化提交命令,首先设置qiming用户免密码登录

#### 使用ssh-keygen命令生成一对秘钥

`$ ssh-keyger`

#### 执行ssh-keygen会在家目录生成两个文件,公钥和私钥

`$ ssh-copy-id qiming@ip`

`$ cd ~`

`$ vim .bash`
``alias cls='clear'``
``alias ali='ssh qinimg@ip'``

刷新该文件
`$ source ./.bash`
`$ ali`

#### 此时无需密码,输入别名即可登录了

### git 免密码提交

#### 回到项目根目录

`$ cd localhost`

`$ vim auto.sh`
``#!/bin/sh``
``git add . && git commit -m "$1" && git push origin master``

### 执行 ./auto.sh '提交'

`$ ./auto.sh '提交'`



