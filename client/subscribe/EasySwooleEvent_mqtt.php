<?php


namespace EasySwoole\EasySwoole;


use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\FastCache\Cache;
use EasySwoole\ORM\DbManager;
use EasySwoole\ORM\Db\Connection;
use App\TcpServer\Service;
use Simps\MQTT\Protocol\Types;
use Simps\MQTT\Protocol\V5;
use Simps\MQTT\Tools\Common;

class EasySwooleEvent implements Event
{
    public static function initialize()
    {
        date_default_timezone_set('Asia/Shanghai');
        
        // 实现 onRequest 事件
        \EasySwoole\Component\Di::getInstance()->set(\EasySwoole\EasySwoole\SysConst::HTTP_GLOBAL_ON_REQUEST, function (\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response): bool {
            ###### 对请求进行拦截 ######
            // 不建议在这拦截请求，可增加一个控制器基类进行拦截
            // 如果真要拦截，判断之后 return false; 即可
            /*
            $code = $request->getRequestParam('code');
            if (0){ // empty($code)验证失败
                $data = array(
                    "code" => \EasySwoole\Http\Message\Status::CODE_BAD_REQUEST,
                    "result" => [],
                    "msg" => '验证失败'
                );
                $response->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                $response->withHeader('Content-type', 'application/json;charset=utf-8');
                $response->withStatus(\EasySwoole\Http\Message\Status::CODE_BAD_REQUEST);
                return false;
            }
            return true;
            */

            ###### 处理请求的跨域问题 ######
            $response->withHeader('Access-Control-Allow-Origin', '*');
            $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            $response->withHeader('Access-Control-Allow-Credentials', 'true');
            $response->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
            if ($request->getMethod() === 'OPTIONS') {
                $response->withStatus(\EasySwoole\Http\Message\Status::CODE_OK);
                return false;
            }
            return true;
        });
        
        // 实现 afterRequest 事件
        \EasySwoole\Component\Di::getInstance()->set(\EasySwoole\EasySwoole\SysConst::HTTP_GLOBAL_AFTER_REQUEST, function (\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response): void {

            // 示例：获取此次请求响应的内容
            //TrackerManager::getInstance()->getTracker()->endPoint('request');
            $responseMsg = $response->getBody()->__toString();
            Logger::getInstance()->console('响应内容:' . $responseMsg);
            // 响应状态码：
            // var_dump($response->getStatusCode());

            // tracker 结束，结束之后，能看到中途设置的参数，调用栈的运行情况
            //TrackerManager::getInstance()->closeTracker();
        });
    }

    public static function mainServerCreate(EventRegister $register)
    {
         ###### 注册 mysql orm 连接池 ######
        $config = new \EasySwoole\ORM\Db\Config(Config::getInstance()->getConf('MYSQL'));
        // 【可选操作】我们已经在 dev.php 中进行了配置
        # $config->setMaxObjectNum(20); // 配置连接池最大数量
        DbManager::getInstance()->addConnection(new Connection($config));
        
        //子服务，另开一个端口进行tcp监听
        $server = \EasySwoole\EasySwoole\ServerManager::getInstance()->getSwooleServer();
        $subPort = $server->addlistener('0.0.0.0', 9510, SWOOLE_BASE);
        $subPort->set([
            // swoole 相关配置
            'open_mqtt_protocol' => true,
            'worker_num' => 2,
            'package_max_length' => 2 * 1024 * 1024,
        ]);
        
        //客户端连接
        $subPort->on($register::onConnect, function (\Swoole\Server $server, int $fd, int $reactor_id) {
                echo "Client #{$fd}: Connected.\n";
        });
        
        //处理收到的消息
        $service = new Service();
        $subPort->on($register::onReceive, function (\Swoole\Server  $server, int $fd, int $reactor_id, $data)use($service) {
            try {
                // debug
                // Common::printf($data);
                $data = V5::unpack($data);
                if (is_array($data) && isset($data['type'])) {
                    switch ($data['type']) {
                        case Types::CONNECT:
                            // Check protocol_name
                            if ($data['protocol_name'] != 'MQTT') {
                                $server->close($fd);
        
                                return false;
                            }
        
                            // Check connection information, etc.
        
                            $server->send(
                                $fd,
                                V5::pack(
                                    [
                                        'type' => Types::CONNACK,
                                        'code' => 0,
                                        'session_present' => 0,
                                        'properties' => [
                                            'maximum_packet_size' => 1048576,
                                            'retain_available' => true,
                                            'shared_subscription_available' => true,
                                            'subscription_identifier_available' => true,
                                            'topic_alias_maximum' => 65535, //0
                                            'wildcard_subscription_available' => true,
                                        ],
                                    ]
                                )
                            );
                            break;
                        case Types::PINGREQ:
                            $server->send($fd, V5::pack(['type' => Types::PINGRESP]));
                            break;
                        case Types::DISCONNECT:
                            if ($server->exist($fd)) {
                                $server->close($fd);
                            }
                            break;
                        case Types::PUBLISH:
                            // Send to subscribers
                            $service->parse($server, $fd, $reactor_id, $data);
                            foreach ($server->connections as $sub_fd) {
                                if ($sub_fd != $fd) {
                                    $server->send(
                                        $sub_fd,
                                        V5::pack(
                                            [
                                                'type' => $data['type'],
                                                'topic' => $data['topic'],
                                                'message' => $data['message'],
                                                'dup' => $data['dup'],
                                                'qos' => $data['qos'],
                                                'retain' => $data['retain'],
                                                'message_id' => $data['message_id'] ?? '',
                                            ]
                                        )
                                    );
                                }
                            }
        
                            if ($data['qos'] === 1) {
                                $server->send(
                                    $fd,
                                    V5::pack(
                                        [
                                            'type' => Types::PUBACK,
                                            'message_id' => $data['message_id'] ?? '',
                                        ]
                                    )
                                );
                            }
        
                            break;
                        case Types::SUBSCRIBE:
                            $payload = [];
                            foreach ($data['topics'] as $k => $option) {
                                $qos = $option['qos'];
                                if (is_numeric($qos) && $qos < 3) {
                                    $payload[] = $qos;
                                } else {
                                    $payload[] = \Simps\MQTT\Hex\ReasonCode::QOS_NOT_SUPPORTED;
                                }
                            }
                            $server->send(
                                $fd,
                                V5::pack(
                                    [
                                        'type' => Types::SUBACK,
                                        'message_id' => $data['message_id'] ?? '',
                                        'codes' => $payload,
                                    ]
                                )
                            );
                            break;
                        case Types::UNSUBSCRIBE:
                            $payload = [];
                            foreach ($data['topics'] as $k => $qos) {
                                if (is_numeric($qos) && $qos < 3) {
                                    $payload[] = $qos;
                                } else {
                                    $payload[] = 0x80;
                                }
                            }
                            $server->send(
                                $fd,
                                V5::pack(
                                    [
                                        'type' => Types::UNSUBACK,
                                        'message_id' => $data['message_id'] ?? '',
                                        'codes' => $payload,
                                    ]
                                )
                            );
                            break;
                    }
                } else {
                    $server->close($fd);
                }
            } catch (\Throwable $e) {
                echo "\033[0;31mError: {$e->getMessage()}\033[0m\r\n";
                $server->close($fd);
            }
        });

        $subPort->on($register::onClose, function (\Swoole\Server  $server, int $fd, int $reactor_id) {
                echo "Client #{$fd}: Closed.\n";
        });

    }
}