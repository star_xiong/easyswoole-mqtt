<?php


namespace EasySwoole\EasySwoole;


use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\FastCache\Cache;
use EasySwoole\ORM\DbManager;
use EasySwoole\ORM\Db\Connection;
use App\TcpServer\Service;
use Simps\MQTT\Client;
use Simps\MQTT\Hex\ReasonCode;
use Simps\MQTT\Protocol\Types;
use Swoole\Coroutine;
use Simps\MQTT\Config\ClientConfig;

class EasySwooleEvent implements Event
{
    const SSL_CERTS_DIR = __DIR__ . '/ssl_certs';
    const TESTS_DIR = __DIR__ . '/../tests';
    
    const SWOOLE_MQTT_CONFIG = [
        'open_mqtt_protocol' => true,
        'package_max_length' => 2 * 1024 * 1024,
        'connect_timeout' => 5.0,
        'write_timeout' => 5.0,
        'read_timeout' => 5.0,
    ];
    
    const SIMPS_MQTT_LOCAL_HOST = '127.0.0.1';
    const SIMPS_MQTT_REMOTE_HOST = 'broker.emqx.io';
    const SIMPS_MQTT_PORT = 9510;

    public static function initialize()
    {
        date_default_timezone_set('Asia/Shanghai');
        
        // 实现 onRequest 事件
        \EasySwoole\Component\Di::getInstance()->set(\EasySwoole\EasySwoole\SysConst::HTTP_GLOBAL_ON_REQUEST, function (\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response): bool {
            ###### 对请求进行拦截 ######
            // 不建议在这拦截请求，可增加一个控制器基类进行拦截
            // 如果真要拦截，判断之后 return false; 即可
            /*
            $code = $request->getRequestParam('code');
            if (0){ // empty($code)验证失败
                $data = array(
                    "code" => \EasySwoole\Http\Message\Status::CODE_BAD_REQUEST,
                    "result" => [],
                    "msg" => '验证失败'
                );
                $response->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                $response->withHeader('Content-type', 'application/json;charset=utf-8');
                $response->withStatus(\EasySwoole\Http\Message\Status::CODE_BAD_REQUEST);
                return false;
            }
            return true;
            */

            ###### 处理请求的跨域问题 ######
            $response->withHeader('Access-Control-Allow-Origin', '*');
            $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            $response->withHeader('Access-Control-Allow-Credentials', 'true');
            $response->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
            if ($request->getMethod() === 'OPTIONS') {
                $response->withStatus(\EasySwoole\Http\Message\Status::CODE_OK);
                return false;
            }
            return true;
        });
        
        // 实现 afterRequest 事件
        \EasySwoole\Component\Di::getInstance()->set(\EasySwoole\EasySwoole\SysConst::HTTP_GLOBAL_AFTER_REQUEST, function (\EasySwoole\Http\Request $request, \EasySwoole\Http\Response $response): void {

            // 示例：获取此次请求响应的内容
            //TrackerManager::getInstance()->getTracker()->endPoint('request');
            $responseMsg = $response->getBody()->__toString();
            Logger::getInstance()->console('响应内容:' . $responseMsg);
            // 响应状态码：
            // var_dump($response->getStatusCode());

            // tracker 结束，结束之后，能看到中途设置的参数，调用栈的运行情况
            //TrackerManager::getInstance()->closeTracker();
        });
    }

    public static function mainServerCreate(EventRegister $register)
    {
         ###### 注册 mysql orm 连接池 ######
        $config = new \EasySwoole\ORM\Db\Config(Config::getInstance()->getConf('MYSQL'));
        // 【可选操作】我们已经在 dev.php 中进行了配置
        # $config->setMaxObjectNum(20); // 配置连接池最大数量
        DbManager::getInstance()->addConnection(new Connection($config));
        
        //子服务，另开一个端口进行tcp监听
        $server = \EasySwoole\EasySwoole\ServerManager::getInstance()->getSwooleServer();
        
        
        //处理订阅到的消息
        $service = new Service();
        
        Coroutine\run(function () {
            $client = new Client(self::SIMPS_MQTT_LOCAL_HOST, self::SIMPS_MQTT_PORT, self::getTestMQTT5ConnectConfig());
            $will = [
                'topic' => 'simps-mqtt/user001/delete',
                'qos' => 1,
                'retain' => 0,
                'message' => 'byebye',
                'properties' => [
                    'will_delay_interval' => 60,
                    'message_expiry_interval' => 60,
                    'content_type' => 'test',
                    'payload_format_indicator' => true, // false 0 1
                ],
            ];
            $client->connect(true, $will);
            $topics['simps-mqtt/user001/get'] = [
                'qos' => 1,
                'no_local' => true,
                'retain_as_published' => true,
                'retain_handling' => 2,
            ];
            $topics['simps-mqtt/user001/update'] = [
                'qos' => 2,
                'no_local' => false,
                'retain_as_published' => true,
                'retain_handling' => 2,
            ];
            $res = $client->subscribe($topics);
            $timeSincePing = time();
            var_dump($res);
            while (true) {
                try {
                    $buffer = $client->recv();
                    if ($buffer && $buffer !== true) {
                        var_dump($buffer);
                        // QoS1 PUBACK
                        if ($buffer['type'] === Types::PUBLISH && $buffer['qos'] === 1) {
                            $client->send(
                                [
                                    'type' => Types::PUBACK,
                                    'message_id' => $buffer['message_id'],
                                ],
                                false
                            );
                        }
                        if ($buffer['type'] === Types::DISCONNECT) {
                            echo sprintf(
                                "Broker is disconnected, The reason is %s [%d]\n",
                                ReasonCode::getReasonPhrase($buffer['code']),
                                $buffer['code']
                            );
                            $client->close($buffer['code']);
                            break;
                        }
                    }
                    if ($timeSincePing <= (time() - $client->getConfig()->getKeepAlive())) {
                        $buffer = $client->ping();
                        if ($buffer) {
                            echo 'send ping success' . PHP_EOL;
                            $timeSincePing = time();
                        }
                    }
                } catch (\Throwable $e) {
                    throw $e;
                }
            }
        });

    }
    
    public static function getTestConnectConfig()
    {
        $config = new ClientConfig();
    
        return $config->setUserName('')
            ->setPassword('')
            ->setClientId(Client::genClientID())
            ->setKeepAlive(10)
            ->setDelay(3000) // 3s
            ->setMaxAttempts(5)
            ->setSwooleConfig(self::SWOOLE_MQTT_CONFIG);
    }
    
    public static function getTestMQTT5ConnectConfig()
    {
        $config = new ClientConfig();
    
        return $config->setUserName('')
            ->setPassword('888888')
            ->setClientId(Client::genClientID())
            ->setKeepAlive(10)
            ->setDelay(3000) // 3s
            ->setMaxAttempts(5)
            ->setProperties([
                'session_expiry_interval' => 60,
                'receive_maximum' => 65535,
                'topic_alias_maximum' => 65535,
            ])
            ->setProtocolLevel(5)
            ->setSwooleConfig(self::SWOOLE_MQTT_CONFIG);
    }
}