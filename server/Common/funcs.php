<?php

    /**
     * 获取毫秒级别的时间戳[20210706100511115]
     */
    function getMillisecond()
    {
        //获取毫秒的时间戳
        $time = explode ( " ", microtime () );
        $time = date('YmdHis',$time[1]) . $time[0]*1000;
        $time2 = explode( ".", $time );
        $time = $time2[0];
        return $time;
    }
    
    /**
     * 生成crc16校验码
     * 用0补齐4位
     */
    function crc($data) {
        $crc = 0xFFFF;
        for($i = 0; $i < strlen ( $data ); $i ++) {
            $crc = ($crc>>8) ^ ord ($data [$i]);
            for($j = 0; $j <8; $j++) {
                if (($crc & 0x0001) != 0) {
                    $crc >>= 1;
                    $crc ^= 0xA001;
                } else{
                    $crc >>= 1;
                }
            }
        }
        
        return str_pad(strtoupper(dechex($crc)),4,0,STR_PAD_LEFT);
    }
    
    /**
     * 产生随机字符串，不长于32位
     * @param int $length
     * @return string 产生的随机字符串
     */
    function getNonceStr(int $length = 32):string
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str ="";
        for ( $i = 0; $i < $length; $i++ )  {
            $str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }
    
    /**
     * 转json，常用于返回数据给前端
     * @param int $status
     * @param array $data
     * @param string $msg
     * @return json 产生的随机字符串
     */
    function asJson(int $status, array $data, string $msg = '操作成功')
    {
        $array = array('code'=>$status, 'data'=>$data, 'msg'=>$msg);
    
        return json($array);
    }
    
 
    /**
     * 格式化十六进制为字符串
     */
    function hex_dump($data, $newline="\n")
    {
        static $from = '';
        static $to = '';
     
        static $width = 16; # number of bytes per line
     
        static $pad = '.'; # padding for non-visible characters
     
        if ($from==='')
        {
            for ($i=0; $i<=0xFF; $i++)
            {
            $from .= chr($i);
            $to .= ($i >= 0x20 && $i <= 0x7E) ? chr($i) : $pad;
            }
        }
     
        $hex = str_split(bin2hex($data), $width*2);
        $chars = str_split(strtr($data, $from, $to), $width);
     
        $offset = 0;
        foreach ($hex as $i => $line)
        {
            //echo sprintf('%6X',$offset).' : '.implode(' ', str_split($line,2)) . ' [' . $chars[$i] . ']' . $newline;
            echo  implode(' ', str_split($line,2)) . $newline;
            $offset += $width;
        }
    }
    
    /**
    * 十六进制浮点型转为十进制
    * @param String $strHex 十六进制浮点数
    */
    function hexToDecFloat($strHex) {
        $v = hexdec($strHex);
        $x = ($v & ((1 << 23) - 1)) + (1 << 23) * ($v >> 31 | 1);
        $exp = ($v >> 23 & 0xFF) - 127;
        return $x * pow(2, $exp - 23);
    }
    
    /**
     * 解析传感器读数 转换为十进制数
     * @param String $value 十六进制字符串
     * @return array $array 返回数组[十进制]
     */
    function parseValue($value,$topic){
        if(empty($value)){
            return [];
        }

        $value =  str_split($value);
        // if(count($value) == 18){
        // if($topic == "污水检测井"){    
        //     /**水质分析 电导率
        //      * 应答帧: 01     03    04   72  37  41  DB  C1 F5
        //      *         地址 功能码       C   D   A   B   CRC16
        //      * CDAB换顺序为ABCD, 上面72 37 41 DB 转为 41 DB 72 37 转浮点为27.4
        //      * 
        //      */
            
        //     return [
        //                 hexdec($value[0].$value[1]),
        //                 round(hexToDecFloat($value[10].$value[11].$value[12].$value[13].$value[6].$value[7].$value[8].$value[9]),2)
        //             ];
        // }
        // else if(count($value) == 26){
        if($topic == "污水检测井" && count($value) > 25){
            /** PHG-206A、DDM-206A
             *  PHG-206A 请求帧：01 03 00 00 00 04 45 BE
             *  PHG-206A 应答帧: 01   03 08    00 62     00 02    01 01   00 01   24 59        十六进制
             *                    ^             ^          ^        ^       ^          
             *                    |             |          |        |       |          
             *                   地址          pH值     小数位   温度值  小数位       
             *                                 0.98     2位小数   25.7   1位小数               十进制
             * ----------------------------------------------------------------------------------------
             *  DDM-206A 请求帧：02 03 00 00 00 04 45 BE
             *  DDM-206A 应答帧: 02   03 08    01 02     00 01    00 B0   00 01   90 48        十六进制
             *                    ^              ^         ^        ^       ^          
             *                    |              |         |        |       |          
             *                   地址        电导率值   小数位   温度值  小数位       
             *                                 25.8     1位小数   17.6   1位小数               十进制
             */

            return [
                        hexdec($value[0].$value[1]),
                        bcdiv(hexdec($value[6].$value[7].$value[8].$value[9]), str_pad(1,hexdec($value[10].$value[11].$value[12].$value[13])+1,0,STR_PAD_RIGHT), hexdec($value[10].$value[11].$value[12].$value[13])),
                        bcdiv(hexdec($value[14].$value[15].$value[16].$value[17]), str_pad(1,hexdec($value[18].$value[19].$value[20].$value[21])+1,0,STR_PAD_RIGHT),hexdec($value[18].$value[19].$value[20].$value[21]))
                    ];
        }
        else if($topic == "零星废水产生企业"){
            $add = hexdec($value[0].$value[1]);
            if($add == 3 || $add == 4 || $add == 5){
                //超声水表:1 030304 00006c48 f4c5    取一位小数
                //return [$add, bcdiv(hexdec($value[6].$value[7].$value[8].$value[9].$value[10].$value[11].$value[12].$value[13]), 10, 1)];
                
                
                //超声水表:2 MODBUS-RTU 通信协议 V1.1 
                /**
                 * 主站请求帧： 03 03 00 01 00 06 94 08 
                 * 从站响应帧： 03 03 0C 00 02 00 32 00 00 00 00 00 00 00 01 14 69
                 * 请求帧：     03 03 00 02 00 04 e4 2b
                 * 响应帧：     03 03 08 00 17 00 00 00 00 00 00 f9 6e
                 * 01 03 0C     : 地址、功能码、字节计数                                                        [0-5]
                 * 00 02        : 累计流量倍率0x0002表示×0.01m³                                                 [6-9]
                 * 00 32 00 00  : 累计流量低字节在前，得到0x00000032（50），累计流量实际值为：50×0.01 = 0.5m³   [10-17] [6-11]
                 * 00 00 00 00  : 瞬时流量（IEEE754）0m³/h                                                      [18-25] [12-17]
                 * 00 01        : 仪表状态字:0x0001 (状态：电池欠压)                                            [26-29] [18-21]
                 * 14 69        : CRC                                                                           [30-33]
                 */
                // 累计流量倍率
                
                if(count($value)>25){
                    // 主站请求帧： 03 03 00 01 00 06 94 08 
                    // 从站响应帧： 03 03 0C 00 02 00 32 00 00 00 00 00 00 00 01 14 69
                    
                    //$A = hexdec($value[6].$value[7].$value[8].$value[9]);
                    //$B = hexdec($value[10].$value[11].$value[12].$value[13].$value[14].$value[15].$value[16].$value[17])/(10*$A);     //累计流量
                    //$C = hexdec($value[18].$value[19].$value[20].$value[21].$value[22].$value[23].$value[24].$value[25]);     //瞬时流量
                    //$D = hexdec($value[26].$value[27].$value[28].$value[29]);     //仪表状态字
                    
                    // 请求帧：     03 03 00 02 00 04 e4 2b
                    // 响应帧：     03 03 08   00 17 00 00   00 00 00 00 f9 6e
                    $B = bcdiv(hexdec($value[10].$value[11].$value[12].$value[13].$value[6].$value[7].$value[8].$value[9]),100,2);         //累计流量
                    $C = bcdiv(hexdec($value[18].$value[19].$value[20].$value[21].$value[14].$value[15].$value[16].$value[17]),100,2)*3600;     //瞬时流量

                    return [$add, $B, $C];
                }
                else{
                    return [$add, 0];
                }
                
            }
            else if($add == 1) {
                //液位 01030214817724
                return [$add, bcdiv(hexdec($value[6].$value[7].$value[8].$value[9]),1000,3)];
            }
            else{
                return [$add, 0];
            }
        }
    }
    
    /**
     * 组装因子及取值为字符串
     * @param array $row 十六进制字符串
     * @param bool $is 是否为实时数据
     * @return string $factor_val 返回数组[十进制]
     */
    function FactorValue($row,$is=false,$data=[]){
        $factor_val = '';
        $data_arr = [];
        if($data && $data['data']){
            $temp_arr = explode(';',$data['data']);
            if($temp_arr){
                foreach($temp_arr as $t_v){
                    $tArra = explode('=',$t_v);
                    if($tArra){
                        if(isset($tArra[1]) && $tArra[1]){
                            $val = explode(',',$tArra[1]);
                            $val = $val[0];
                        }
                        else{
                            $val =0;
                        }
                       $data_arr[$tArra[0]] = $val;
                    }
                }
            }
        }

        if($row['device']['task'] == "重点排水户末端监控"){
            $factor = $row['device']['factor'];
            $value = $row['values'];
            
            // 污水检测井PH
            $i = 0;
            if($factor[$i]['name'] == "污水检测井PH值" || $factor[$i]['name'] == "雨水监测井PH值"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value,$n,$is);
            }
            
            // 污水检测井电导率
            $i = 1;
            if($factor[$i]['name'] == "污水检测井电导率" || $factor[$i]['name'] == "雨水监测井电导率"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value,$n,$is);
            }
        }
        else if($row['device']['task'] == "零星废水产生企业") {
            
            // var_dump($row);
            
            $factor = $row['device']['factor'];
            $value = $row['values'];
            // if(count($value)<2){
            //     return '';
            // }

            // 水池液位高度
            $i = 1;
            if(isset($value[$i])){
                $n = 1;
                $v = $factor[0]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
                
                // 零星废水储量
                $val = [];
                $val['v'.$n] = $value[$i]['v1']*$value[$i]['mianji'];
                
                if(!$is){
                    $val['minv'.$n] = $value[$i]['minv'.$n]*$value[$i]['mianji'];
                    $val['maxv'.$n] = $value[$i]['maxv'.$n]*$value[$i]['mianji'];
                }

                $factor_val .= factor_val($factor[1]['code'], $val, $n, $is);
                
            }
            
            // 零星废水储量
            // $i = 2;
            // if(isset($value[$i])){
            //     $n = 1;
            //     $v = $factor[1]['code'];
            //     $factor_val .= factor_val($v,$value[$i],$i,$n,$is);
            // }
            
            // 工业用水瞬时流量1
            $i = 3;
            if(isset($value[$i])){
                $n = 1;
                $v = $factor[2]['code'];
                
                $factor_val .= factor_val($v,$value[$i],2,$is);
                
                //累计流量
                $v = $factor[3]['code'];
                if(!$is && isset($data_arr['w10001-Avg'])){
                    $factor_val .= 'w10001-Cou='.($value[$i]['v'.$n]-$data_arr['w10001-Avg']).',';
                }
                $factor_val .= factor_val($v,$value[$i],$n,$is);
                
            }
            
            // 工业用水瞬时流量2
            $i = 4;
            if(isset($value[$i])){
                $n = 1;
                $v = $factor[4]['code'];
                $factor_val .= factor_val($v,$value[$i],2,$is);
                
                //累计流量
                $v = $factor[6]['code'];
                if(!$is && isset($data_arr['w10002-Avg'])){
                    $factor_val .= 'w10002-Cou='.($value[$i]['v'.$n]-$data_arr['w10002-Avg']).',';
                }
                
                $factor_val .= factor_val($v,$value[$i],$n,$is);
                
            }
            
            // 工业用水瞬时流量3
            $i = 5;
            if(isset($value[$i])){
                $n = 1;
                $v = $factor[5]['code'];
                $factor_val .= factor_val($v,$value[$i],2,$is);
                
                //累计流量
                $v = $factor[7]['code'];
                if(!$is && isset($data_arr['w10003-Avg'])){
                    $factor_val .= 'w10003-Cou='.($value[$i]['v'.$n]-$data_arr['w10003-Avg']).',';
                }
                $factor_val .= factor_val($v,$value[$i],$n,$is);
                
            }
            // 工业用水流量
            // $i = 4;
            // if(isset($value[$i])){
            //     $n = 1;
            //     $v = $factor[3]['code'];
            //     $factor_val .= factor_val($v,$value[$i],$n,$is);
            // }
        }
        else if($row['device']['task'] == "市级以上VOCs过程监控") {
            $factor = $row['device']['factor'];
            $value = $row['values'];
            
            // VOCs排放设施电流
            $i = 0;
            if($factor[$i]['name'] == "VOCs排放设施电流"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
            }
            
            // VOCs排放温度
            $i = 1;
            if($factor[$i]['name'] == "VOCs排放温度"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
            }
            
            // VOCs排放瞬时流量
            $i = 2;
            if($factor[$i]['name'] == "VOCs排放瞬时流量"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
            }
            
            // 废气治理设备用电电流
            $i = 3;
            if($factor[$i]['name'] == "废气治理设备用电电流"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
            }
            
            // 涉VOCs生产用电功率
            $i = 4;
            if($factor[$i]['name'] == "涉VOCs生产用电功率"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
            }
            
            // VOCs排放压力
            $i = 5;
            if($factor[$i]['name'] == "VOCs排放压力"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
            }
            
            // 活性炭吸附进出口压力差
            $i = 6;
            if($factor[$i]['name'] == "活性炭吸附进出口压力差"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
            }
            
            // 涉VOCs生产用电量
            $i = 7;
            if($factor[$i]['name'] == "涉VOCs生产用电量"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
            }
            
            // VOCs浓度
            $i = 8;
            if($factor[$i]['name'] == "VOCs浓度"){
                $n = 1;
                $v = $factor[$i]['code'];
                $factor_val .= factor_val($v,$value[$i],$n,$is);
                // if($value[$i]){
                //     $val = $value[$i];
                //     $factor_val .= $v.'-Min='.$val['minv'.$n].','.$v.'-Avg='.$val['v'.$n].','.$v.'-Max='.$val['maxv'.$n].','.$v.'-Flag=N;';
                // }
                // else{
                //     $factor_val .= $v.'-Min=0,'.$v.'-Avg=0,'.$v.'-Max=0,'.$v.'-Flag=N;';
                // }
            }
        }
        return $factor_val;
    }
    

    function factor_val($v,$val,$n,$is){
        if($is){
            //##0229QN=20210911110730001;ST=32;CN=2011;PW=123456;MN=LX202109110003;Flag=4;CP=&&DataTime=20210911110700;we1101-Rtd=15.4375,we1101-Flag=N;we0001-Rtd=0.3211,we0001-Flag=N;w10001-Rtd=0.2300,w10001-Flag=N;w10101-Rtd=0.0000,w10101-Flag=N&&E381
            if($val){
                $factor_val = $v.'-Rtd='.$val['v'.$n].','.$v.'-Flag=N;';
            }
            else{
                $factor_val = $v.'-Rtd=0,'.$v.'-Flag=N;';
            }
        }
        else{
            if($val){
                $factor_val = $v.'-Min='.$val['minv'.$n].','.$v.'-Avg='.$val['v'.$n].','.$v.'-Max='.$val['maxv'.$n].','.$v.'-Flag=N;';
            }
            else{
                $factor_val = $v.'-Min=0,'.$v.'-Avg=0,'.$v.'-Max=0,'.$v.'-Flag=N;';
            }
        }
        return $factor_val;
    }
        