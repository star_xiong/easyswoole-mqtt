<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/10/26
 * Time: 5:39 PM
 */

namespace App\HttpController\Api\Admin;

use App\Model\Admin\AdminModel;
use EasySwoole\Http\Message\Status;
use EasySwoole\HttpAnnotation\AnnotationTag\Param;
use EasySwoole\Jwt\Jwt;

class Auth extends AdminBase
{
    protected $whiteList=['login'];
    protected $exp = 60*60*2;
    protected $key = "hainenghuanjin";

    /**
     * login
     * 登陆,参数验证注解写法
     * @Param(name="account", alias="帐号", required="", lengthMax="20")
     * @Param(name="password", alias="密码", required="", lengthMin="6", lengthMax="16")
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     * @author Tioncico
     * Time: 10:18
     */
    function login_bak()
    {
        $param = $this->request()->getRequestParam();
        $model = new AdminModel();
        $model->adminAccount = $param['account'];
        $model->adminPassword = md5($param['password']);

        if ($user = $model->login()) {
            $sessionHash = md5(time() . $user->adminId);
            $user->update([
                'adminLastLoginTime' => time(),
                'adminLastLoginIp'   => $this->clientRealIP(),
                'adminSession'       => $sessionHash
            ]);

            $rs = $user->toArray();
            unset($rs['adminPassword']);
            $rs['adminSession'] = $sessionHash;
            $this->response()->setCookie('adminSession', $sessionHash, time() + 3600, '/');
            $this->writeJson(Status::CODE_OK, $rs);
        } else {
            $this->writeJson(Status::CODE_BAD_REQUEST, '', '密码错误');
        }

    }
    
    public function login()
    {
        $param = $this->request()->getRequestParam();
        if(empty($param['account']) || empty($param['password'])){
            $result = ['code' => Status::CODE_BAD_REQUEST, 'data' => $param ];
            return $this->writeJson(Status::CODE_OK, $result, '账号或密码不能为空');
            
        }
        
        $model = new AdminModel();
        $model->adminAccount = $param['account'];
        if ($user = $model->login()) {
            # 判断用户密码是否正确
            if(password_verify($param['password'],$user->adminPassword)){
                # 权限
                // if($this->request()->getHeader('origin')[0] == "http://admin.changoeducation.com" && $user !="admin"){
                //     return $this->writeJson(Status::CODE_OK,$user,'没有权限');
                // }

                //登录成功，生成用户登录token
                $time = time();
                $token = array('id'=>$user->adminId,'account'=>$user->adminAccount,'iat'=>$time,'exp'=>$time+$this->exp,'nbf'=>$time);
                $jwtObject = Jwt::getInstance()
                        ->setSecretKey($this->key) // 秘钥
                        ->publish();
                $jwtObject->setAlg('HMACSHA256'); // 加密方式
                $jwtObject->setAud($user->adminAccount ); // 用户
                $jwtObject->setExp($time+$exp); // 过期时间
                $jwtObject->setIat($time); // 发布时间
                $jwtObject->setIss('easyswoole'); // 发行人
                $jwtObject->setJti(md5($time)); // jwt id 用于标识该jwt
                $jwtObject->setNbf($time+60*5); // 在此之前不可用
                $jwtObject->setSub('HaiNeng'); // 主题
                
                // 自定义数据
                $jwtObject->setData([
                    'id'=>$user->adminId,
                    'account'=>$user->adminAccount
                ]);
                
                // 最终生成的token
                $token = $jwtObject->__toString();
                $this->response()->setCookie('adminSession', $token, time() + $exp, '/');
                $result = ['code' => Status::CODE_OK, 'data' => $token ];
                return $this->writeJson(Status::CODE_OK,$result,'登录成功');
            }

        }

        //账号或者密码不正确
        $result = ['code' => Status::CODE_BAD_REQUEST, 'data' => $param ];
        return $this->writeJson(Status::CODE_OK, $result,'账号或密码不正确');
    }

    /**
     * logout
     * 退出登录,参数注解写法
     * @Param(name="adminSession", from={COOKIE}, required="")
     * @return bool
     * @author Tioncico
     * Time: 10:23
     */
    function logout()
    {
        $sessionKey = $this->request()->getRequestParam($this->sessionKey);
        if (empty($sessionKey)) {
            $sessionKey = $this->request()->getCookieParams('adminSession');
        }
        if (empty($sessionKey)) {
            $this->writeJson(Status::CODE_UNAUTHORIZED, '', '尚未登入');
            return false;
        }
        $result = $this->getWho()->logout();
        if ($result) {
            $this->writeJson(Status::CODE_OK, '', "登出成功");
        } else {
            $this->writeJson(Status::CODE_UNAUTHORIZED, '', 'fail');
        }
    }

    function getInfo()
    {
        $this->writeJson(200, $this->getWho()->toArray(), 'success');
    }
}
