<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/10/26
 * Time: 5:39 PM
 */

namespace App\HttpController\Api\Admin;

use App\HttpController\Api\ApiBase;
use App\Model\Admin\AdminModel;
use EasySwoole\Http\Message\Status;
use EasySwoole\Validate\Validate;
use EasySwoole\Jwt\Jwt;

class AdminBase extends ApiBase
{
    //public才会根据协程清除
    public $who;
    //session的cookie头
    protected $sessionKey = 'adminSession';
    //白名单
    protected $whiteList = [];

    /**
     * onRequest
     * @param null|string $action
     * @return bool|null
     * @throws \Throwable
     * @author yangzhenyu
     * Time: 13:49
     */
    function onRequest(?string $action): ?bool
    {
        if (parent::onRequest($action)) {
            //白名单判断
            if (in_array($action, $this->whiteList)) {
                return true;
            }
            //获取登入信息
            if (!$this->getWho()) {
                $this->writeJson(Status::CODE_UNAUTHORIZED, '', '登入已过期');
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * getWho
     * @return bool
     * @author yangzhenyu
     * Time: 13:51
     */
    function getWho(): ?AdminModel
    {
        if ($this->who instanceof AdminModel) {
            return $this->who;
        }
        
        $token = $this->request()->getRequestParam('token');
        if(empty($token)){
            if(empty($this->request()->getHeaders()['token'])){
                return $this->writeJson(200,'','未登录');
            }

            $token = $this->request()->getHeaders()['token'];
            if(!empty($token)){
                $token = $token[0];
            }else{
                return $this->writeJson(200,'','未登录');
            }
        }

        
        try {
            $jwtObject = Jwt::getInstance()->decode($token);
        
            $status = $jwtObject->getStatus();
        
            // 如果encode设置了秘钥,decode 的时候要指定
            // $status = $jwt->setSecretKey('easyswoole')->decode($token)
        
            switch ($status)
            {
                case  1:
                    //$jwtObject->getAlg();
                    //$jwtObject->getAud();
                    $user = $jwtObject->getData();
                    // $jwtObject->getExp();
                    // $jwtObject->getIat();
                    // $jwtObject->getIss();
                    // $jwtObject->getNbf();
                    // $jwtObject->getJti();
                    // $jwtObject->getSub();
                    // $jwtObject->getSignature();
                    // $jwtObject->getProperty('alg');
                    break;
                case  -1:
                    return $this->writeJson(200,'','未登录');
                    break;
                case  -2:
                    return $this->writeJson(200,'','未登录');
                break;
            }
        } catch (\EasySwoole\Jwt\Exception $e) {
            return $this->writeJson(200,'','未登录');
        }
        
        
        $sessionKey = $this->request()->getRequestParam($this->sessionKey);
        if (empty($sessionKey)) {
            $sessionKey = $this->request()->getCookieParams($this->sessionKey);
        }
        if (empty($sessionKey)) {
            return null;
        }
        $adminModel = new AdminModel();
        $adminModel->adminSession = $sessionKey;
        $this->who = $adminModel->getOneBySession();
        return $this->who;
    }

    function getWho_bak(): ?AdminModel
    {
        if ($this->who instanceof AdminModel) {
            return $this->who;
        }
        $sessionKey = $this->request()->getRequestParam($this->sessionKey);
        if (empty($sessionKey)) {
            $sessionKey = $this->request()->getCookieParams($this->sessionKey);
        }
        if (empty($sessionKey)) {
            return null;
        }
        $adminModel = new AdminModel();
        $adminModel->adminSession = $sessionKey;
        $this->who = $adminModel->getOneBySession();
        return $this->who;
    }
    
    protected function getValidateRule(?string $action): ?Validate
    {
        return null;
        // TODO: Implement getValidateRule() method.
    }
}
