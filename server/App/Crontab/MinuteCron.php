<?php
/**
 * Created by PhpStorm.
 * User: Star Xiong
 * Date: 2021/04/21
 * Time: 16:15
 */
namespace App\Crontab;

use EasySwoole\EasySwoole\Task\TaskManager;
use EasySwoole\EasySwoole\Crontab\AbstractCronTask;
use EasySwoole\Component\Di;
use App\Model\DeviceRecordModel;
use App\Model\SendDataModel;
use EasySwoole\EasySwoole\Logger;
// use EasySwoole\Queue\Job;
// use App\Utility\MyQueue;
// use App\Model\OrderModel;
/**
 * Class  MinuteCron
 * 每10分钟发送一次数据
 * @package App\Crontab
*/
class MinuteCron extends AbstractCronTask
{

    public static function getRule(): string
    {
        // 定义执行规则 根据Crontab来定义
        $crontab = '*/10 * * * *';   //每十分钟执行一次
        
        return $crontab;
    }

    public static function getTaskName(): string
    {
        // 定时任务的名称
        return 'MinuteCron';
    }

    public function run(int $taskId, int $workerIndex)
    {
        // 定时任务的执行逻辑

        // 开发者可投递给task异步处理
        TaskManager::getInstance()->async(function (){
            
            //$this->test();
            
            //10分钟数据
            $QN = getMillisecond();
            $dataTime =  intdiv(date('YmdHi00',strtotime("-10 minute")), 1000)*1000;
            // $dataTime = "20210701102300";      2021-07-30 16:11:32
            // $dataTime = "20210730161000";
            $max = intdiv(date('YmdHi00'), 1000)*1000;
            $data = DeviceRecordModel::getRecord($dataTime,$max);
            
            if(!empty($data)){
                foreach($data as $key=>$row){
                    
                    if(empty($row['device']['factor'])){
                        continue;
                    }
                    $per_data = SendDataModel::create()->where('device_id',$key)
                                                                ->where('type','i')
                                                                ->order('time','DESC')
                                                                ->get()
                                                                ->toArray();
                    $factor_val = FactorValue($row,false,$per_data);
                    
                    if(empty($factor_val)){
                        continue;
                    }
                    
                    $msg = "QN={$QN};ST=32;CN=2051;PW={$row['device']['pw']};MN={$row['device']['mn']};Flag=4;CP=&&DataTime={$dataTime};{$factor_val}&&";
                    
                    $len = str_pad(strlen($msg),4,0,STR_PAD_LEFT);
                    $crc = crc($msg);
                    $msg = "##{$len}{$msg}{$crc}\r\n";
                    
                    var_dump($msg);
                    // Logger::getInstance()->info($msg);
                    //保存发送的数据
                    $send_data = [
                            'type'=>'m',
                            'time'=>$dataTime,
                            'device_id'=>$key,
                            'data'=>$msg
                        ];
                    
                    SendDataModel::create()->data($send_data)->save();
                    // 向自定义进程中传输信息，会触发自定义进程的 onPipeReadable 回调
            
                    
                    // 获取 Di 中注入的 自定义进程
                    $post = 6000;
                    if($row['device']['topic'] == '零星废水产生企业'){
                        $post = 5600;
                    }
                    $processName = "TcpClient{$post}";
                    $tcpClientProcess = Di::getInstance()->get($processName);
                    $flag = $tcpClientProcess->write($msg);
                    
                    
                    echo "Flag:{$flag}\n";
            
                }
            }
            
/*            
            if(false){
                foreach($data as $row){
                    
                    var_dump($row);
                    //因子
                    $yinzhi = explode(",",$row['device']['yinzi']);
                    
                    // if(empty($yinzhi)){
                    //     continue;
                    // }
                    
                    $yzStr = '';            
                    //wc1001-Min={$row['minv1']},wc1001-Avg={$row['v1']},wc1001-Max={$row['maxv1']},wc1001-Flag=N;wd1201-Min={$row['minv2']},wd1201-Avg={$row['v2']},wd1201-Max={$row['maxv2']},wd1201-Flag=N;ea2001-Min={$row['minv3']},ea2001-Avg={$row['v3']},ea2001-Max={$row['maxv3']},ea2001-Cou=0,ea2001-Flag=N
                    foreach($yinzhi as $k=>$v){
                        $n = $k+1;
                        $yzStr .= $v.'-Min='.$row['minv'.$n].','.$v.'-Avg='.$row['v'.$n].','.$v.'-Max='.$row['maxv'.$n].','.$v.'-Flag=N;';
                    }
                    $msg = "QN={$QN};ST=32;CN=2051;PW={$row['device']['pw']};MN={$row['device']['mn']};Flag=4;CP=&&DataTime={$dataTime};{$yzStr}&&";
                    $len = str_pad(strlen($msg),4,0,STR_PAD_LEFT);
                    $crc = crc($msg);
                    $msg = "##{$len}{$msg}{$crc}\r\n";
                    var_dump($msg);
                    // 向自定义进程中传输信息，会触发自定义进程的 onPipeReadable 回调
            
                    
                    // 获取 Di 中注入的 自定义进程
                    $post = 6000;
                    $processName = "TcpClient{$post}";
                    $tcpClientProcess = Di::getInstance()->get($processName);
                    $flag = $tcpClientProcess->write($msg);
                    echo "Flag:{$flag}\n";
            
                }
            }
            
            */
            // $msg = "QN={$QN};ST=32;CN=2051;PW=123456;MN=PS202107020460;Flag=4;CP=&&DataTime={$dataTime};wc1001-Min=0.00,wc1001-Avg=0.00,wc1001-Max=0.00,wc1001-Flag=N;wd1201-Min=120.6,wd1201-Avg=120.9,wd1201-Max=121.4,wd1201-Flag=N;ea2001-Min=12.06,ea2001-Avg=12.09,ea2001-Max=12.14,ea2001-Cou=0,ea2001-Flag=N&&";
            // $len = str_pad(strlen($msg),4,0,STR_PAD_LEFT);
            // $crc = crc($msg);
            // $msg = "##{$len}{$msg}{$crc}\r\n";

            // // 向自定义进程中传输信息，会触发自定义进程的 onPipeReadable 回调
            
            
            // // 获取 Di 中注入的 自定义进程
            // $post = 6000;
            // $processName = "TcpClient{$post}";
            // $tcpClientProcess = Di::getInstance()->get($processName);
            // $flag = $tcpClientProcess->write($msg);
            // echo "Flag:{$flag}\n";
            
        });
    }

    private function test(){
        // PH:wd1001、电导率：wd1201
        
        //10分钟数据
        $mn = "PS202107300010";
        $QN = getMillisecond();
        $dataTime =  intdiv(date('YmdHi00',strtotime("-10 minute")), 1000)*1000;
        //$dataTime ="20210701102300";
        $max = intdiv(date('YmdHi00'), 1000)*1000;
        $data = DeviceRecordModel::getRecord($dataTime,$max);
        $row = [
                'v1' => 7.56,
                'minv1' => 7.51,
                'maxv1' => 7.62,
                'v2' => 19,
                'minv2' => 18,
                'maxv2' => 21,
                'v3' => 0,
                'minv3' => 0,
                'maxv3' => 0,
            ];
         $msg = "QN={$QN};ST=32;CN=2051;PW=123456;MN={$mn};Flag=4;CP=&&DataTime={$dataTime};wd1001-Min={$row['minv1']},wd1001-Avg={$row['v1']},wd1001-Max={$row['maxv1']},wd1001-Flag=N;wd1201-Min={$row['minv2']},wd1201-Avg={$row['v2']},wd1201-Max={$row['maxv2']},wd1201-Flag=N&&";
        $len = str_pad(strlen($msg),4,0,STR_PAD_LEFT);
        $crc = crc($msg);
        $msg = "##{$len}{$msg}{$crc}\r\n";
        
        // 向自定义进程中传输信息，会触发自定义进程的 onPipeReadable 回调

        
        // 获取 Di 中注入的 自定义进程
        $post = 6000;
        $processName = "TcpClient{$post}";
        $tcpClientProcess = Di::getInstance()->get($processName);
        $flag = $tcpClientProcess->write($msg);
        echo "Flag:{$flag}\n";
            
        
    }
    
    private function sendMessage($msg){
        
            
    }

    public function onException(\Throwable $throwable, int $taskId, int $workerIndex)
    {
        // 捕获run方法内所抛出的异常
        echo $throwable->getMessage();
    }
    
    
    
    
}