<?php
/**
 * Created by PhpStorm.
 * User: Star Xiong
 * Date: 2021/04/21
 * Time: 16:15
 */
namespace App\Crontab;

use EasySwoole\EasySwoole\Task\TaskManager;
use EasySwoole\EasySwoole\Crontab\AbstractCronTask;
use EasySwoole\Component\Di;
// use EasySwoole\Queue\Job;
// use App\Utility\MyQueue;
use App\Model\DeviceRecordModel;
/**
 * Class  MinuteCron
 * 每1分钟发送一次数据
 * @package App\Crontab
*/
class InstantData extends AbstractCronTask
{

    public static function getRule(): string
    {
        // 定义执行规则 根据Crontab来定义
        $crontab = '*/1 * * * *';   //每1分钟执行一次
        
        return $crontab;
    }

    public static function getTaskName(): string
    {
        // 定时任务的名称
        return 'InstantData';
    }

    public function run(int $taskId, int $workerIndex)
    {
        // 定时任务的执行逻辑

        // 开发者可投递给task异步处理
        TaskManager::getInstance()->async(function (){
            
            //$this->test();
            
            $QN = getMillisecond();

            $dataTime = date('YmdHi00',strtotime("-1 minute"));
            $max = date('YmdHi00');
            //$dataTime ="20210701102300";
            $data = DeviceRecordModel::getRecord($dataTime,$max);
            if(false && !empty($data)){
                foreach($data as $row){
                    
                    if(empty($row['device']['factor'])){
                        continue;
                    }
                    
                    $factor_val = FactorValue($row);
                    
                    if(empty($factor_val)){
                        continue;
                    }
                    
                    $msg = "QN={$QN};ST=32;CN=2011;PW={$row['device']['pw']};MN={$row['device']['mn']};Flag=4;CP=&&DataTime={$dataTime};{$factor_val}&&";
                    
                    // $msg = "QN={$QN};ST=32;CN=2011;PW=123456;MN=PS202107020460;Flag=4;CP=&&DataTime={$dataTime};wc1001-Rtd={$row['v1']},wc1001-Flag=B;wc1001-Flag=B;wd1201-Rtd={$row['v2']},wd1201-Flag=N;wd1201-Flag=N;ea2001-Rtd={$row['v1']},ea2001-Flag=N;ea2001-Flag=N&&";
                    $len = str_pad(strlen($msg),4,0,STR_PAD_LEFT);
                    $crc = crc($msg);
                    $msg = "##{$len}{$msg}{$crc}\r\n";
                    
                    var_dump($msg);
                    
                    // // 向自定义进程中传输信息，会触发自定义进程的 onPipeReadable 回调
                    // // 获取 Di 中注入的 自定义进程
                    // $post = 6000;
                    // if($row['device']['topic'] == '零星废水产生企业'){
                    //     $post = 5600;
                    // }
                    // $processName = "TcpClient{$post}";
                    // $tcpClientProcess = Di::getInstance()->get($processName);
                    // $flag = $tcpClientProcess->write($msg);
                    // echo "Flag:{$flag}\n";
                }
            }

            
            //解析传感器读数 转换为十进制数
            //$value = "01030800620002010100012459";
            
            //0x01,0x03,0x08,0x00,0x62,0x00,0x02,0x01,0x01,0x00,0x01,0x24,0x59
            // $valueArray = parseValue($value);
            // if($valueArray){
            //     var_dump($valueArray);
            // }
            
            //var_dump(parseValue("010304723741DBC1F5"));
            
            //0203080102000100B000019048
        });
    }

    private function test(){
        // PH:wd1001、电导率：wd1201
        
        //10分钟数据
        $mn = "PS202107300010";
        $QN = getMillisecond();
        $dataTime =  intdiv(date('YmdHi00',strtotime("-10 minute")), 1000)*1000;
        //$dataTime ="20210701102300";
        $max = intdiv(date('YmdHi00'), 1000)*1000;
        $data = DeviceRecordModel::getRecord($dataTime,$max);
        $row = [
                'v1' => 7.56,
                'minv1' => 7.51,
                'maxv1' => 7.62,
                'v2' => 19,
                'minv2' => 18,
                'maxv2' => 21,
                'v3' => 0,
                'minv3' => 0,
                'maxv3' => 0,
            ];
         $msg = "QN={$QN};ST=32;CN=2051;PW=123456;MN={$mn};Flag=4;CP=&&DataTime={$dataTime};wd1001-Min={$row['minv1']},wd1001-Avg={$row['v1']},wd1001-Max={$row['maxv1']},wd1001-Flag=N;wd1201-Min={$row['minv2']},wd1201-Avg={$row['v2']},wd1201-Max={$row['maxv2']},wd1201-Flag=N&&";
        $len = str_pad(strlen($msg),4,0,STR_PAD_LEFT);
        $crc = crc($msg);
        $msg = "##{$len}{$msg}{$crc}\r\n";
        
        // 向自定义进程中传输信息，会触发自定义进程的 onPipeReadable 回调

        
        // 获取 Di 中注入的 自定义进程
        $post = 6000;
        $processName = "TcpClient{$post}";
        $tcpClientProcess = Di::getInstance()->get($processName);
        $flag = $tcpClientProcess->write($msg);
        echo "Flag:{$flag}\n";
            
        
    }

    private function sendMessage($msg){
        
            
    }

    public function onException(\Throwable $throwable, int $taskId, int $workerIndex)
    {
        // 捕获run方法内所抛出的异常
        echo $throwable->getMessage();
    }
    
}