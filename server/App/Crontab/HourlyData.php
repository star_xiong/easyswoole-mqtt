<?php
/**
 * Created by PhpStorm.
 * User: Star Xiong
 * Date: 2021/04/21
 * Time: 16:15
 */
namespace App\Crontab;

use EasySwoole\EasySwoole\Task\TaskManager;
use EasySwoole\EasySwoole\Crontab\AbstractCronTask;
use EasySwoole\Component\Di;
use App\Model\DeviceRecordModel;
use App\Model\SendDataModel;
// use EasySwoole\Queue\Job;
// use App\Utility\MyQueue;
// use App\Model\OrderModel;
/**
 * Class  MinuteCron
 * 每小时发送一次数据
 * @package App\Crontab
*/
class HourlyData extends AbstractCronTask
{

    public static function getRule(): string
    {
        // 定义执行规则 根据Crontab来定义
        $crontab = '0 * * * *';   //每小时执行一次
        
        return $crontab;
    }

    public static function getTaskName(): string
    {
        // 定时任务的名称
        return 'HourlyData';
    }

    public function run(int $taskId, int $workerIndex)
    {
        // 定时任务的执行逻辑

        // 开发者可投递给task异步处理
        TaskManager::getInstance()->async(function (){
            $QN = getMillisecond();
            
            $dataTime =  date('YmdH0000',strtotime("-1 hour"));
            $max = date('YmdH0000');
            $data = DeviceRecordModel::getRecord($dataTime,$max);
            
            if(!empty($data)){
                foreach($data as $key=>$row){
                    
                    if(empty($row['device']['factor'])){
                        continue;
                    }
                    
                    $factor_val = FactorValue($row);
                    
                    if(empty($factor_val)){
                        continue;
                    }
                    
                    $msg = "QN={$QN};ST=32;CN=2061;PW={$row['device']['pw']};MN={$row['device']['mn']};Flag=4;CP=&&DataTime={$dataTime};{$factor_val}&&";
                    
                    //$msg = "QN={$QN};ST=32;CN=2061;PW=123456;MN=PS202107020460;Flag=4;CP=&&DataTime={$dataTime};wc1001-Min=0.00,wc1001-Avg=0.00,wc1001-Max=0.00,wc1001-Flag=N;wd1201-Min=120.6,wd1201-Avg=120.9,wd1201-Max=121.4,wd1201-Flag=N;ea2001-Min=12.06,ea2001-Avg=12.09,ea2001-Max=12.14,ea2001-Cou=0,ea2001-Flag=N&&";
                    $len = str_pad(strlen($msg),4,0,STR_PAD_LEFT);
                    $crc = crc($msg);
                    $msg = "##{$len}{$msg}{$crc}\r\n";
                    
                    //保存发送的数据
                    $send_data = [
                            'type'=>'h',
                            'time'=>$dataTime,
                            'device_id'=>$key,
                            'data'=>$msg
                        ];
                    
                    SendDataModel::create()->data($send_data)->save();
                    
                    // 向自定义进程中传输信息，会触发自定义进程的 onPipeReadable 回调
                    // 获取 Di 中注入的 自定义进程
                    $post = 6000;
                    if($row['device']['topic'] == '零星废水产生企业'){
                        $post = 5600;
                    }
                    $processName = "TcpClient{$post}";
                    $tcpClientProcess = Di::getInstance()->get($processName);
                    $flag = $tcpClientProcess->write($msg);
                    echo "Flag:{$flag}\n";
            
                }
            }

        });
    }


    private function sendMessage($msg){
        
            
    }

    public function onException(\Throwable $throwable, int $taskId, int $workerIndex)
    {
        // 捕获run方法内所抛出的异常
        echo $throwable->getMessage();
    }
    
    
    
    
}