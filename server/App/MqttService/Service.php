<?php
namespace App\MqttService;

use App\Model\DeviceModel;
use App\Model\User\UserModel;
use App\Model\DeviceRecordModel;
use EasySwoole\Component\Di;
use App\Model\SendDataModel;

class Service
{

    public function parse(\Swoole\Server  $server, int $fd, int $reactor_id, $data)
    {
        
        // $post = 5600;
        // $processName = "TcpClient{$post}";
        // $tcpClientProcess = Di::getInstance()->get($processName);
        // $flag = $tcpClientProcess->write($msg);
        
        // $result = DeviceModel::create()->all();
        // var_dump($data);
        $allow = array('零星废水产生企业','污水检测井');
        if(in_array($data['topic'], $allow)){
                $message = bin2hex($data['message']);       //函数把包含数据的二进制字符串转换为十六进制值字符串
                
                //$message = $data['message'];              //MQTT.fx测试用 十六进制值字符串
                
                $data['message'] = $message;
                echo "fd:{$fd} reactor_id:{$reactor_id} send:".json_encode($data)."\n";
                
                $redis = \EasySwoole\RedisPool\RedisPool::defer();
                $record = [
                    'device_id' => $redis->get("fd_device_id_{$fd}"),
                    'imei' => $redis->get("fd_client_id_{$fd}"),
                    'value' => $message,
                    'topic' => $data['topic'],
                    'created_at' => time()
                    ];

                $msgArray = parseValue($message,$data['topic']);
                
                foreach($msgArray as $key=>$val){
                    if($key == 0){
                        $record['address'] = $val;
                    }
                    else{
                        $record['v'.$key] = $val;
                    }
                }    

                DeviceRecordModel::create()->data($record)->save();
                
                //实时数据
                if($data['topic'] == '零星废水产生企业'){
                    $QN = getMillisecond();

                    $dataTime = date('YmdHi',strtotime("-1 minute"));
                    $max = date('YmdHi');
                    $s = date('s',time());
                    if($s<30){
                        $s='00';
                    }
                    else{
                        $s='30';
                    }
                    $dataTime = $dataTime.$s;
                    $max = $max.$s;
                    // var_dump('$max=>',$max);
                    //$dataTime ="20210701102300";
                    $data = DeviceRecordModel::getRecord($dataTime,$max,$data['topic']);

                    if(!empty($data)){
                        foreach($data as $key=>$row){
                            
                            if(empty($row['device']['factor'])){
                                continue;
                            }
                            
                            // $per_data = SendDataModel::create()->where('device_id',$key)
                            //                                     ->where('type','i')
                            //                                     ->order('time','DESC')
                            //                                     ->get()
                            //                                     ->toArray();
                            
                            $factor_val = FactorValue($row,true);
                            
                            if(empty($factor_val)){
                                continue;
                            }
                            
                            $msg = "QN={$QN};ST=32;CN=2011;PW={$row['device']['pw']};MN={$row['device']['mn']};Flag=4;CP=&&DataTime={$dataTime};{$factor_val}&&";
                            //##0229QN=20210911110730001;ST=32;CN=2011;PW=123456;MN=LX202109110003;Flag=4;CP=&&DataTime=20210911110700;we1101-Rtd=15.4375,we1101-Flag=N;we0001-Rtd=0.3211,we0001-Flag=N;w10001-Rtd=0.2300,w10001-Flag=N;w10101-Rtd=0.0000,w10101-Flag=N&&E381
                            $len = str_pad(strlen($msg),4,0,STR_PAD_LEFT);
                            $crc = crc($msg);
                            $msg = "##{$len}{$msg}{$crc}\r\n";
                            
                            var_dump($msg);
                            $send_data = [
                                'type'=>'i',
                                'time'=>$dataTime,
                                'device_id'=>$key,
                                'data'=>$msg
                            ];
                        
                            SendDataModel::create()->data($send_data)->save();
                            
                            // 向自定义进程中传输信息，会触发自定义进程的 onPipeReadable 回调
                            // 获取 Di 中注入的 自定义进程
                            $post = 5600;
                            $processName = "TcpClient{$post}";
                            $tcpClientProcess = Di::getInstance()->get($processName);
                            $flag = $tcpClientProcess->write($msg);
                            echo "Flag:{$flag}\n";
                        }
                    }
                }
                // $time = date('YmdHis');
                // $QN = $this->getMillisecond();
                // $msg = "QN={$QN};ST=32;CN=2011;PW=123456;MN=PS202107020460;Flag=4;CP=&&DataTime={$time};wd1001-Rtd=7.00,wd1001-Flag=B;wd1201-Rtd=0,wd1201-Flag=N&&";
                // $len = strlen($msg);
                // $crc = $this->crc($msg);
                // $msg = "##{$len}{$msg}{$crc}\r\n";
                // $tcpClientProcess->write($msg);
                // echo $msg;
                
               
                //$client->send($msg);
                
                // $post = '9510';
                // $client_id = '8.129.131.22';
                // // $post = '5600';
                // // $client_id = '119.145.97.35';
                
                // $client->sendto($client_id,$post,$msg);
                //$report = $client->recv();
                //echo "{$report}\n";
                

            
        }
        
    }
    
    public function login(int $fd, int $reactor_id, $data)
    {
        var_dump($data);
        if(empty($data['user_name']) || empty($data['password'])){
            return false;
        }

        $device = DeviceModel::create()->get(['mn'=>$data['user_name']]);
        if(empty($device) || $device['pw'] != $data['password']){
            return false;
        }
        
        $device->update([
            'last_login' => time()
        ]);

        $redis = \EasySwoole\RedisPool\RedisPool::defer();
        $redis->set("fd_client_id_{$fd}", $data['client_id']);
        $redis->set("fd_device_id_{$fd}",$device['id']);
        echo "fd:{$fd} Login\n";
        return true;
    }
    
    public function login_bak(int $fd, int $reactor_id, $data)
    {
        if(empty($data['user_name']) || empty($data['password'])){
            return false;
        }

        $user = UserModel::create()->get(['userAccount'=>$data['user_name']]);
        if(empty($user) || $user['userPassword'] != md5($data['password'])){
            return false;
        }
        
        $user->update([
            'lastLoginTime' => time()
        ]);

        $redis = \EasySwoole\RedisPool\RedisPool::defer();
        $redis->set("fd_client_id_{$fd}", $data['client_id']);
        $redis->set("fd_device_id_{$fd}",$user['userId']);
        echo "fd:{$fd} Login\n";
        return true;
    }
}
