<?php
namespace App\MqttService;

use EasySwoole\Component\Process\AbstractProcess;
use Swoole\Process;

class TcpClient extends AbstractProcess
{
    protected $client;
    
    public function dolog($msg, $filename = '/Users/xxx/sites/release_log.log')
    {
        if ($msg) {
            error_log($msg . PHP_EOL, 3, $filename);
        }
    }
    
    protected function run($arg)
    {
        // TODO: Implement run() method.
        $processName = $this->getProcessName(); // 获取 注册进程名称
        $swooleProcess = $this->getProcess(); // 获取 注册进程的实例 \Swoole\Process
        $processPid = $this->getPid(); // 获取 当前进程 Pid
        $args = $this->getArg(); // 获取 注册进程时传递的参数
        var_dump('### 开始运行自定义进程 start ###');
        if($args['clientId'] && $args['post'] && $args['timeout']){
            //tcp客户端
            $this->client = new \Swoole\Client(SWOOLE_SOCK_TCP);
            // $client->set(
            //     [
            //         'timeout' => 5,//总超时，包括连接、发送、接收所有超时
            //         'connect_timeout' => 5.0,//连接超时，会覆盖第一个总的 timeout
            //         'write_timeout' => 5.0,//发送超时，会覆盖第一个总的 timeout
            //         'read_timeout' => 5.0,//接收超时，会覆盖第一个总的 timeout
            //     ]
            // );
    
            if (!$this->client->connect($args['clientId'], $args['post'], $args['timeout'])) {
                echo "connect failed. Error: {$this->client->errCode}\n";
            }
            
            //发送心跳10秒一次
            // \EasySwoole\Component\Timer::getInstance()->loop(10 * 1000, function () {
            //     echo "this timer runs at intervals of 10 seconds\n";
            //     $flag ="FF FF FF FF FF FF FF FF";
                
            //     //$msg = "##0234QN=20210705114504373;ST=32;CN=2011;PW=123456;MN=PS202107020460;Flag=4;CP=&&DataTime=20210705114500;wc1001-Rtd=0.00,wc1001-Flag=B;wc1001-Flag=B;wd1201-Rtd=120.5,wd1201-Flag=N;wd1201-Flag=N;ea2001-Rtd=12.05,ea2001-Flag=N;ea2001-Flag=N&&3600";
    
                
            //     $this->client->send($flag);
            // });
        }
        
        var_dump($processName, $swooleProcess, $processPid, $args);
        var_dump('### 运行自定义进程结束 end ###');
    }

    protected function onPipeReadable(Process $process)
    {
        // 该回调可选
        // 当主进程对子进程发送消息的时候 会触发
        $recvMsgFromMain = $process->read(); // 用于获取主进程给当前进程发送的消息
        var_dump("收到主进程发送的消息: {$recvMsgFromMain}");
        $this->client->send($recvMsgFromMain);
        
    }

    protected function onException(\Throwable $throwable, ...$args)
    {
        // 该回调可选
        // 捕获 run 方法内抛出的异常
        // 这里可以通过记录异常信息来帮助更加方便地知道出现问题的代码
        
    }

    protected function onShutDown()
    {
        // 该回调可选
        // 进程意外退出 触发此回调
        // 大部分用于清理工作
    }

    protected function onSigTerm()
    {
        // 当进程接收到 SIGTERM 信号触发该回调
    }
}