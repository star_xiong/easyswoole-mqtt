<?php

namespace App\Model;

use EasySwoole\ORM\Utility\Schema\Table;
use EasySwoole\ORM\AbstractModel;

/**
 * Class AdminModel
 * Create With Automatic Generator
 * @property $adminId
 * @property $adminName
 * @property $adminAccount
 * @property $adminPassword
 * @property $adminSession
 * @property $adminLastLoginTime
 * @property $adminLastLoginIp
 */
class SendDataModel extends AbstractModel
{
    protected $tableName = 'send_data';

    protected $primaryKey = 'id';
    
    public function schemaInfo(bool $isCache = true): Table
    {
        $table = new Table($this->tableName);
        $table->colInt('id')->setIsPrimaryKey(true);
        $table->colInt('device_id');
        $table->colChar('data');
        $table->colChar('type');
        $table->colChar('time');
        return $table;
    }
    
   
}