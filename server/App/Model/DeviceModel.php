<?php

namespace App\Model;

use EasySwoole\ORM\AbstractModel;
use EasySwoole\ORM\Utility\Schema\Table;
/**
 * Class AdminModel
 * Create With Automatic Generator
 * @property $id
 * @property $uid
 */
class DeviceModel extends AbstractModel
{
    protected $tableName = 'device';

    protected $primaryKey = 'id';
    
    public function schemaInfo(bool $isCache = true): Table
    {
        $table = new Table($this->tableName);
        $table->colInt('id')->setIsPrimaryKey(true);
        $table->colInt('uid');
        $table->colChar('imei');
        $table->colChar('task');
        $table->colChar('type');
        $table->colChar('mn');
        $table->colChar('pw');
        $table->colChar('brand');
        $table->colChar('online');
        $table->colChar('authentication');
        $table->colInt('created_at');
        $table->colInt('last_login');
        
        return $table;
    }
}