<?php

namespace App\Model;

use EasySwoole\ORM\AbstractModel;
use EasySwoole\ORM\Utility\Schema\Table;
/**
 * Class AdminModel
 * Create With Automatic Generator
 * @property $id
 */
class FactorModel extends AbstractModel
{
    protected $tableName = 'factor';

    protected $primaryKey = 'id';
    
    public function schemaInfo(bool $isCache = true): Table
    {
        $table = new Table($this->tableName);
        $table->colInt('id')->setIsPrimaryKey(true);
        $table->colChar('task');
        $table->colChar('type');
        $table->colChar('name');
        $table->colChar('code');
        
        return $table;
    }
}