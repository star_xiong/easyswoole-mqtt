<?php

namespace App\Model;

use EasySwoole\ORM\Utility\Schema\Table;
use EasySwoole\ORM\AbstractModel;

/**
 * Class AdminModel
 * Create With Automatic Generator
 * @property $adminId
 * @property $adminName
 * @property $adminAccount
 * @property $adminPassword
 * @property $adminSession
 * @property $adminLastLoginTime
 * @property $adminLastLoginIp
 */
class DeviceRecordModel extends AbstractModel
{
    protected $tableName = 'device_record';

    protected $primaryKey = 'id';
    
    public function schemaInfo(bool $isCache = true): Table
    {
        $table = new Table($this->tableName);
        $table->colInt('id')->setIsPrimaryKey(true);
        $table->colInt('device_id');
        $table->colChar('imei');
        $table->colChar('value');
        $table->colChar('address');
        $table->colChar('v1');
        $table->colChar('v2');
        $table->colChar('v3');
        $table->colChar('v4');
        $table->colChar('v5');
        $table->colChar('v6');
        $table->colChar('v7');
        $table->colChar('topic');
        $table->colChar('created_at');
        return $table;
    }
    
    static public function getRecord($min,$max,$topic=''){
        $sql = "select dr.device_id, dr.imei, dr.address, dr.topic, d.mianji, d.pw, d.mn, d.task, d.type,
                        MIN(dr.v1) as minv1, 
                        MIN(dr.v2) as minv2, 
                        MIN(dr.v3) as minv3, 
                        MIN(dr.v4) as minv4, 
                        MIN(dr.v5) as minv5, 
                        MIN(dr.v6) as minv6, 
                        MIN(dr.v7) as minv7,
                        MAX(dr.v1) as maxv1, 
                        MAX(dr.v2) as maxv2, 
                        MAX(dr.v3) as maxv3, 
                        MAX(dr.v4) as maxv4, 
                        MAX(dr.v5) as maxv5, 
                        MAX(dr.v6) as maxv6, 
                        MAX(dr.v7) as maxv7,
                        ROUND(AVG(dr.v1),2) as v1, 
                        ROUND(AVG(dr.v2),2) as v2, 
                        ROUND(AVG(dr.v3),2) as v3, 
                        ROUND(AVG(dr.v4),2) as v4, 
                        ROUND(AVG(dr.v5),2) as v5, 
                        ROUND(AVG(dr.v6),2) as v6, 
                        ROUND(AVG(dr.v7),2) as v7
                        from device_record dr 
                        left join device d on d.id = dr.device_id
                        left join user_list u on u.userId = d.uid
                        where dr.address<6 and FROM_UNIXTIME(dr.created_at, '%Y%m%d%H%i00') >= {$min} and FROM_UNIXTIME(dr.created_at, '%Y%m%d%H%i00') < {$max}
                        ";
        if($topic){
            $sql .= " and dr.topic='{$topic}'";
        }
        $sql .= " group by dr.device_id,dr.address order by dr.device_id,dr.address";
        $data = self::create()->func(function ($builder) use($sql){
            $builder->raw($sql,[1]);
            return true;
        });
        
        $factor = FactorModel::create()->all();
        $return_factor = [];
        foreach($factor as $item){
            if(empty($item->type)){
                $return_factor[$item->task][] = $item->toRawArray();
            }
            else{
                $return_factor[$item->task][$item->type][] = $item->toRawArray();
            }
        }

        //格式化数据：['device_id']['address'] = $row
        $return_array = [];   
        foreach($data as $key=>$row){
            $device = [
                    'task' => $row['task'],
                    'topic' => $row['topic'],
                    'mianji' => $row['mianji'],
                    'type' => $row['type'],
                    'pw' => $row['pw'],
                    'mn' => $row['mn']
                ];
            $device['factor'] = empty($row['type']) ? $return_factor[$row['task']] : $return_factor[$row['task']][$row['type']];
            if(empty($return_array[$row['device_id']]['device'])){
                $return_array[$row['device_id']]['device'] = $device;
            }
            $return_array[$row['device_id']]['values'][$row['address']] = $row;
        }
        return $return_array;
    }
}